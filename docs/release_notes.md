<!-- markdownlint-disable no-emphasis-as-heading-->
# Release Notes

## 0.10.6

__Fixes__:

* Updated manifest.json env var

## 0.10.5

__Enhancements__:

* Added `pixel-data-check` config option; if True, attempts to parse pixel data
and reports if pixel data appears to be corrupted or otherwise
unreadable
* Updated events to be stored as list of dictionary with `tag` and
  `event` as key.

__Fixes__:

* Updated fw-file dependency to fix ValueError when adding elements
to the NonConformingElementSequence

## 0.10.4

__Enhancements__

* Added handling for EOFErrors
* Ensure that black pixel data does not cause gear failure

## 0.10.3

__Enhancements__:

* Ensure new `SeriesInstanceUID` and `StudyInstanceUID` are unique across project

__Maintenance__:

* Increase unit test coverage.

## 0.10.2

__Enhancements__:

* Ensure that ultrasound DICOM images with
`PhotometricInterpretation: PALETTE COLOR` are correctly converted to 8-bit RGB

__Maintenance__:

* Increase unit test coverage.
* Add missing docstrings.

## 0.9.6

__Enhancements__:

* Output file type set to `dicom` in Flywheel metadata.

__Maintenance__:

* Add categorization options to manifest.

## 0.9.2

__Fixes__:

* Changed `strict-validation` default to True.

## 0.9.1

__Enhancements__:

* Added available memory check to catch too-large files before decompression,
so that by default it skips decompression vs being OOM killed.
* Added `force_decompress` config option to force decompression even if the
file may be too big (Expert option).

__Maintenance__:

* Update to `fw-file:3.3.0`
  * Includes tempfix for certain condition values within DICOM validation

## 0.8.3

__Enhancements__:

* New `dicom-standard` config option can be configured as `"local"`
 to use locally saved DICOM Standard (for faster processing) or `"current"`
 to download the most recent DICOM Standard at runtime. Default `"local"`.
* New `strict-validation` config option can be configured as `True` for strict
 DICOM Standard validation or `False` to opt out, leaving python parsable values as-is.
 Default `False`
* Uncompression now utilizes pylibjpeg instead of gdcm
* PatientSex fixer behavior changed: when PatientSex cannot be inferred to M or F or O,
 it is now set as empty. If value is already empty, value is unchanged.

__Maintenance__:

* Update to `fw-file:3.0.0`
  * Improved handling of condition parsing and missing values

## 0.7.4

__Enhancement__:

* Adjust `is_dcm` filter logic.  First looks at file-meta and then looks for presence of
  SOPClassUID.
* Now supports RLE compression.

__Maintenance__:

* Better testing for both `standardize_transfer_syntax` and `is_dcm` dicom filter.

## 0.7.3

__BugFix__:

* `standardize_transfer_syntax` is respected

__Maintanance__:

* Add better tests for standardize transfer syntax

## 0.7.2

__Enhancement__:

* Pull in updates from fw-file
  * Improve handling of invalid CS values
  * Improve handling of invalid DS/IS values

## 0.7.1

__BugFix__:

* Fix writing of post-reading modifications to OAS

## 0.7.0

__Enhancement__:

* Handling of VR mismatch changed to simply rely on dictionary value
  * Includes fix for LO -> PN [GEAR-3458]
  * Includes fix for
* Greatly improved handling of invalid values
  * Conformant with dicom standard instead of simply changing VR to a parsable value.
  * Population of original/modified attributes on reading
  * Differential handling for required and optional elements
  * Will now log error when a required element is invalid.
  * Better fixes for private tags.
* Individual fixes for UIDs
  * Fix for MediaStorageSOPClassUID
  * Strip extra null-bytes

__Maintenance__:

* Update CI to sse-qa-ci

## 0.6.0

__Bug__:

* Ensure no outputs are uploaded if there is an error writing
* Ensure no fixes applied when writing OriginalAttributes

## 0.5.4

__Maintenance__:

* Output to `file.info.qc` namespace

## 0.5.2

__Maintenance__:

* Change config option zip single dicoms to an enum.

## 0.5.1

__Enhancements__:

* Add config option to zip single dicoms
* Add metadata methods from GTK reorganize metadata

## 0.5.0

__Enhancements__:

* Add fixers:
  * Fix `PatientSex` to either `M`, `F`, `O`
  * Crop text VRs to max length
  * Fix decimals in Integer String (IS) VR
  * Regenerate and replace invalid UIDs.
  * Remove invalid characters in DS/IS strings
  * Remove unprintable characters in Text strings
* Add events for post-decoding fixers (incorrect_unit, PatientSex).

__Maintenance__:

* Enable linting `pylint`
* Change fixer format and move `callbacks` to `fixers`
* Update dependencies for CVEs

## 0.4.1

__Fixes__:

* `standardize_transfer_syntax` convert TransferSyntax when no other event are found.

## 0.4.0

__Enhancements__:

* Compressed dicoms will be uncompressed

__Fixes__:

* Fix handling of default encoding in callbacks.

## 0.3.7

__Fixes__:

* Update `is_dcm` to handle repeaters which return `None` for `tag_for_keyword`

## 0.3.6

__Fixes__:

* Update `is_dcm` callback to require at least 2 public tags outside the file_meta group.

## 0.3.5

__Enhancements__:

* Add in filtering on DICOMs.  Filter out files without valid DICOM tags.
* Improve logging when multiple SeriesInstanceUIDs are detected.

## 0.3.4

__Fixes__:

* Force read dicom files.

## 0.3.3

__Fixes__:

* Disable pydicom callbacks when checking if original element can be written.

## 0.3.2

__Fixes__

* Don't attempt to write an element to the ModifiedAttributesSequence
if the original element was invalid.

## 0.3.1

__Fixes__

* Fix exception when modality is not set on input file.

## 0.3.0

__Enhancements__

* Add config option for tag.

## 0.2.1

* Initial release.
