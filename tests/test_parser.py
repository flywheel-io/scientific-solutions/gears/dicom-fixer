"""Module to test parser.py"""

from pathlib import Path
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext
from pydicom import config as pydicom_config

from fw_gear_dicom_fixer.parser import parse_config


@pytest.fixture
def mock_calculate(mocker):
    calc = mocker.patch("fw_gear_dicom_fixer.parser.calculate_decompressed_size")
    return calc


def test_parse_config(tmp_path):
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = {
        "unique": True,
        "standardize_transfer_syntax": False,
        "zip-single-dicom": "match",
        "strict-validation": True,
        "dicom-standard": "current",
        "convert-palette": False,
        "new-uids-needed": False,
        "pixel-data-check": False,
    }
    gear_context.get_input_path.return_value = tmp_path
    (
        dicom,
        transfer,
        unique,
        zip_single,
        new_uids_needed,
        fail_status,
        convert_palette,
        pixel_data_check,
    ) = parse_config(gear_context)

    assert dicom == Path(tmp_path)
    assert not transfer
    assert unique
    assert zip_single == "match"
    assert not fail_status
    assert not new_uids_needed
    assert pydicom_config.settings.reading_validation_mode == 2
    assert not convert_palette
    assert not pixel_data_check


def test_parse_config_transfer_syntax_force(tmp_path, mock_calculate, caplog):
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = {
        "unique": True,
        "standardize_transfer_syntax": True,
        "force_decompress": True,
        "zip-single-dicom": "match",
        "strict-validation": True,
        "dicom-standard": "current",
        "convert-palette": False,
        "new-uids-needed": False,
        "pixel-data-check": False,
    }
    gear_context.get_input_path.return_value = tmp_path
    mock_calculate.return_value = 999999999999999
    (
        dicom,
        transfer,
        unique,
        zip_single,
        new_uids_needed,
        fail_status,
        convert_palette,
        pixel_data_check,
    ) = parse_config(gear_context)

    assert transfer
    assert not fail_status
    assert "continuing as configured" in caplog.text
    assert not convert_palette


def test_parse_config_transfer_syntax_switch(tmp_path, mock_calculate, caplog):
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = {
        "unique": True,
        "standardize_transfer_syntax": True,
        "force_decompress": False,
        "zip-single-dicom": "match",
        "strict-validation": True,
        "dicom-standard": "current",
        "convert-palette": True,
        "pixel-data-check": False,
    }
    gear_context.get_input_path.return_value = tmp_path
    mock_calculate.return_value = 999999999999999
    (
        dicom,
        transfer,
        unique,
        zip_single,
        new_uids_needed,
        fail_status,
        convert_palette,
        pixel_data_check,
    ) = parse_config(gear_context)

    assert not transfer
    assert fail_status
    assert "switched to False" in caplog.text
    assert convert_palette
    # Check that parser returns default "False" for new_uids_needed
    # when not specified in config
    assert not new_uids_needed
