from pathlib import Path

import pytest
from flywheel_gear_toolkit.testing.files import create_dcm
from fw_file import dicom
from fw_file.dicom.config import get_config
from fw_file.dicom.reader import TrackedRawDataElement
from pydicom.dataelem import RawDataElement
from pydicom.tag import TupleTag

ASSETS = Path(__file__).parents[0] / "assets"

pytest_plugins = ("flywheel_gear_toolkit.testing",)


@pytest.fixture(autouse=True)
def config():
    get_config.cache_clear()
    return get_config()


@pytest.fixture
def dcmcoll(tmp_path):
    """Return DICOMCollection with two files."""

    def _gen(**kwargs):
        for key, val in kwargs.items():
            create_dcm(**val, file=str(tmp_path / (key + ".dcm")))
        return dicom.DICOMCollection.from_dir(tmp_path)

    return _gen


@pytest.fixture
def raw_data_elem():
    def gen(my_val, tracked=False, tag=(0x0018, 0x0087), **kwargs):
        # Magnetic Field Strength
        my_kwargs = {
            "VR": "DS",
            "length": len(my_val),
            "value": my_val,
            "value_tell": 0,
            "is_implicit_VR": True,
            "is_little_endian": True,
            "is_raw": True,
        }
        my_kwargs.update(kwargs)
        if tracked:
            return TrackedRawDataElement(TupleTag(tag), **my_kwargs)
        else:
            return RawDataElement(TupleTag(tag), **my_kwargs)

    return gen
