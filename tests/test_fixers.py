import logging

import pytest
from fw_file.dicom import DICOM
from fw_file.dicom.config import get_config
from fw_file.dicom.reader import ReplaceEvent
from pydicom import config as pydicom_config
from pydicom.data import get_testdata_files
from pydicom.uid import ExplicitVRLittleEndian

from fw_gear_dicom_fixer.fixers import (
    LUT_TAGS,
    convert_color_space_fixer,
    fix_incorrect_units,
    fix_invalid_modality,
    fix_patient_sex,
    is_dcm,
    no_dataelem_fixes,
    standardize_transfer_syntax,
)

from .conftest import ASSETS


@pytest.fixture
def mock_decompress(mocker):
    decomp = mocker.patch("pydicom.dataset.Dataset.decompress")
    return decomp


def test_is_dcm(tmp_path, create_dcm_fixture, mocker):
    dcm = create_dcm_fixture()
    test_dcm = str(tmp_path / "test.dcm")
    with open(test_dcm, "w") as fp:
        fp.write("test")
    assert is_dcm(dcm)
    assert not is_dcm(DICOM(test_dcm, force=True))


def test_is_dcm_with_unknown_tags(tmp_path, create_dcm_fixture):
    dcm = create_dcm_fixture()
    # Add OverlayRows repeater
    dcm.dataset.raw.add_new((0x6000, 0x0010), "US", 512)
    assert is_dcm(dcm)


def test_empty_pydicom_callback():
    config = get_config()
    orig_fixers = config.list_fixers()
    orig_replace = pydicom_config.replace_un_with_known_vr
    orig_wrong_len = pydicom_config.convert_wrong_length_to_UN
    try:
        with pytest.raises(ValueError):
            with no_dataelem_fixes():
                config = get_config()
                assert config.raw_elem_fixers == []
                assert not pydicom_config.replace_un_with_known_vr
                assert not pydicom_config.convert_wrong_length_to_UN
                raise ValueError

    finally:
        assert config.list_fixers() == orig_fixers
        assert orig_replace == pydicom_config.replace_un_with_known_vr
        assert orig_wrong_len == pydicom_config.convert_wrong_length_to_UN


@pytest.mark.parametrize(
    "val,corr,evt",
    [
        (3000.0, 3.0, ReplaceEvent("MagneticFieldStrength", 3000.0, 3.0)),
        (3.0, 3.0, None),
    ],
)
def test_handle_incorrect_unit(create_dcm_fixture, val, corr, evt):
    dcm = create_dcm_fixture(**{"MagneticFieldStrength": val})
    out = fix_incorrect_units(dcm)
    assert dcm.MagneticFieldStrength == corr
    assert out == evt


@pytest.mark.parametrize(
    "in_,exp,out_,validation_mode",
    [
        # If it's already correct (no fix needed), validation_mode doesn't change outcome.
        ("M", "M", None, 2),
        ("M", "M", None, 1),
        # If validation_mode==1, "male" does not get changed to MALE before being changed to M.
        ("male", "M", ReplaceEvent("PatientSex", "male", "M"), 1),
        # Similarly, "MRLE" remains "MRLE" through loose validation.
        ("MRLE", "M", ReplaceEvent("PatientSex", "MRLE", "M"), 1),
        # If validation_mode==2, in_ must be CAPS or will raise during validation.
        ("FEMALE", "F", ReplaceEvent("PatientSex", "FEMALE", "F"), 2),
        ("FAMALE", "F", ReplaceEvent("PatientSex", "FAMALE", "F"), 2),
        ("OTHER", "O", ReplaceEvent("PatientSex", "OTHER", "O"), 2),
        ("OTHRE", "O", ReplaceEvent("PatientSex", "OTHRE", "O"), 2),
        ("Anonymized", "", ReplaceEvent("PatientSex", "Anonymized", ""), 1),
        ("ANONYMIZED", "", ReplaceEvent("PatientSex", "ANONYMIZED", ""), 2),
        # The following will raise an error during validation.
        # ("Anonymized", "", ReplaceEvent("PatientSex", "ANONYMIZED", ""),2),
        # If the value is already empty, the fixer shouldn't do anything.
        ("", "", None, 2),
        ("", "", None, 1),
    ],
)
def test_patient_sex_fixer(create_dcm_fixture, in_, out_, exp, validation_mode, config):
    config.reading_validation_mode = validation_mode
    dcm = create_dcm_fixture(**{"PatientSex": in_})
    out = fix_patient_sex(dcm)
    assert out == out_
    assert dcm.PatientSex == exp


def test_patient_sex_warns(create_dcm_fixture):
    dcm = create_dcm_fixture(**{"PatientSex": "ANONYMIZED"})
    with pytest.warns(UserWarning):
        out = fix_patient_sex(dcm)
    assert out == ReplaceEvent("PatientSex", "ANONYMIZED", "")
    assert dcm.PatientSex == ""


@pytest.mark.parametrize(
    "in_,exp,out_",
    [
        ("MR", "MR", None),
        ("MRA", "MR", ReplaceEvent("Modality", "MRA", "MR")),
        ("XXXXXXX", "OT", ReplaceEvent("Modality", "XXXXXXX", "OT")),
        (None, "OT", ReplaceEvent("Modality", "", "OT")),
    ],
)
def test_invalid_modality_fixer(create_dcm_fixture, in_, out_, exp):
    dcm = create_dcm_fixture(**{"Modality": in_})
    out = fix_invalid_modality(dcm)
    assert out == out_
    assert dcm.Modality == exp


def test_invalid_modality_fixer_no_modality(create_dcm_fixture):
    dcm = create_dcm_fixture()
    out = fix_invalid_modality(dcm)
    assert out == ReplaceEvent("Modality", None, "OT")
    assert dcm.Modality == "OT"


@pytest.mark.parametrize("path", get_testdata_files(pattern="**/*.dcm"))
def test_standardize_transfer_syntax(path, config, caplog):
    pytest.importorskip("gdcm")
    config.reading_validation_mode = 1
    dcm = DICOM(path, force=True)
    raw = dcm.dataset.raw
    try:
        ts = raw.file_meta.TransferSyntaxUID
    except AttributeError:
        return
    try:
        standardize_transfer_syntax(dcm)
        ts = raw.file_meta.TransferSyntaxUID
        assert ts == ExplicitVRLittleEndian
        assert not raw.is_implicit_VR
        assert raw.is_little_endian
    except RuntimeError:
        assert "Could not decompress" in caplog.text


def test_standardize_transfer_syntax_attribute_error(config, mock_decompress, caplog):
    config.reading_validation_mode = 1
    test_file = get_testdata_files(pattern="**/SC_rgb_dcmtk_+eb+cy+s2.dcm")[0]
    dcm = DICOM(test_file, force=True)
    mock_decompress.side_effect = AttributeError("Error!")

    with pytest.raises(AttributeError):
        standardize_transfer_syntax(dcm)

    assert "Could not decompress" in caplog.text


@pytest.mark.parametrize("path", get_testdata_files(pattern="**/*.dcm"))
def test_color_space(path):
    dcm = DICOM(path, force=True)
    pytest.importorskip("gdcm")
    try:
        p_i = dcm.PhotometricInterpretation
    except AttributeError:
        return
    sup = [
        "YBR_FULL_422",
        "YBR_FULL",
        "RGB",
        "MONOCHROME1",
        "MONOCHROME2",
        "PALETTE COLOR",
    ]
    if p_i not in sup:
        pytest.skip(f"Cannot handle PhotometricInterpretation {p_i}")

    convert_color_space_fixer(dcm)

    assert dcm.PhotometricInterpretation in ["MONOCHROME1", "MONOCHROME2", "RGB"]


def test_color_space_palette(caplog):
    dcm = DICOM(ASSETS / "palette_color.dcm", force=True)
    dcm.dataset.raw.file_meta.TransferSyntaxUID = ExplicitVRLittleEndian
    assert len(dcm.PixelData) == 307200

    new_color = convert_color_space_fixer(dcm, convert_palette=True)
    assert len(dcm.PixelData) == 1843200

    assert dcm.PhotometricInterpretation == "RGB"
    assert dcm.PlanarConfiguration == 0
    assert dcm.SamplesPerPixel == 3
    assert dcm.BitsAllocated == 16
    assert dcm.BitsStored == 16
    assert dcm.HighBit == 15

    assert dcm.dataset.raw.pixel_array.dtype == "uint16"

    assert all(tag not in dcm for tag in LUT_TAGS)
    assert len(caplog.record_tuples) == 1
    assert "[Palette Color] Changing Bits Allocated from" in caplog.record_tuples[0][2]
    assert caplog.record_tuples[0][1] == logging.WARNING
    assert new_color


def test_color_space_palette_us(caplog):
    dcm = DICOM(ASSETS / "palette_color_US.dcm", force=True)
    dcm.dataset.raw.file_meta.TransferSyntaxUID = ExplicitVRLittleEndian
    assert len(dcm.PixelData) == 307200

    new_color = convert_color_space_fixer(dcm, convert_palette=True)
    assert len(dcm.PixelData) == 921600

    assert dcm.PhotometricInterpretation == "RGB"
    assert dcm.PlanarConfiguration == 0
    assert dcm.SamplesPerPixel == 3
    assert dcm.BitsAllocated == 8
    assert dcm.BitsStored == 8
    assert dcm.HighBit == 7

    assert dcm.dataset.raw.pixel_array.dtype == "uint8"

    assert all(tag not in dcm for tag in LUT_TAGS)
    assert len(caplog.record_tuples) == 1
    assert "Converting 16-bit RGB to 8-bit." in caplog.record_tuples[0][2]
    assert caplog.record_tuples[0][1] == logging.INFO
    assert new_color


def test_no_color_space_palette(caplog):
    dcm = DICOM(ASSETS / "palette_color.dcm", force=True)
    dcm.dataset.raw.file_meta.TransferSyntaxUID = ExplicitVRLittleEndian
    assert len(dcm.PixelData) == 307200

    new_color = convert_color_space_fixer(dcm, convert_palette=False)
    assert len(dcm.PixelData) == 307200

    assert dcm.PhotometricInterpretation == "PALETTE COLOR"
    assert dcm.PlanarConfiguration == 1
    assert dcm.SamplesPerPixel == 1
    assert dcm.BitsAllocated == 8
    assert dcm.BitsStored == 8
    assert dcm.HighBit == 7

    assert dcm.dataset.raw.pixel_array.dtype == "uint8"

    assert all(tag in dcm for tag in LUT_TAGS)
    assert new_color
