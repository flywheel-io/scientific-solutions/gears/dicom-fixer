"""Module to test utils.py"""

import pytest
from fw_file import dicom

from fw_gear_dicom_fixer.utils import calculate_decompressed_size


@pytest.fixture
def mock_sniff(mocker):
    sniff = mocker.patch("fw_gear_dicom_fixer.utils.sniff_dcm")
    return sniff


@pytest.fixture
def mock_is_zip(mocker):
    zip = mocker.patch("fw_gear_dicom_fixer.utils.zipfile.is_zipfile")
    return zip


def test_calculate_decompressed_size_multiple_frames(
    tmp_path, dcmcoll, mocker, mock_sniff, mock_is_zip, config
):
    config.reading_validation_mode = 2
    mock_sniff.return_value = True
    mock_is_zip.return_value = False
    coll = dcmcoll(
        **{
            "test1": {
                "Rows": 2,
                "Columns": 2,
                "NumberOfFrames": 2,
                "SamplesPerPixel": 2,
                "BitsAllocated": 2,
            }
        }
    )
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None

    size = calculate_decompressed_size(tmp_path)

    assert size == 4


def test_calculate_decompressed_size_multiple_dicoms(
    tmp_path, dcmcoll, mocker, mock_sniff, mock_is_zip, config
):
    config.reading_validation_mode = 2
    mock_sniff.return_value = False
    mock_is_zip.return_value = True
    num_dicoms = 2
    coll = dcmcoll(
        **{
            f"test{i}": {
                "Rows": 2,
                "Columns": 2,
                "SamplesPerPixel": 2,
                "BitsAllocated": 2,
            }
            for i in range(num_dicoms)
        }
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll

    size = calculate_decompressed_size(tmp_path)

    assert size == 4


def test_calculate_decompressed_size_multiple_tag_values(
    tmp_path, dcmcoll, mocker, mock_sniff, mock_is_zip, config
):
    config.reading_validation_mode = 2
    mock_sniff.return_value = False
    mock_is_zip.return_value = True
    num_dicoms = 3
    rows_values = [1, 4, None]
    coll = dcmcoll(
        **{
            f"test{i}": {
                "Rows": rows_values[i],
                "Columns": 2,
                "SamplesPerPixel": 2,
                "BitsAllocated": 2,
            }
            for i in range(num_dicoms)
        }
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll

    size = calculate_decompressed_size(tmp_path)

    assert size == 12


def test_calculate_decompressed_size_multiple_tag_vals_and_missing_tag(
    tmp_path, dcmcoll, mocker, mock_sniff, mock_is_zip, caplog, config
):
    config.reading_validation_mode = 2
    mock_sniff.return_value = False
    mock_is_zip.return_value = True
    num_dicoms = 3
    rows_values = [1, 4, 2]
    cols_values = [None, None, None]
    coll = dcmcoll(
        **{
            f"test{i}": {
                "Rows": rows_values[i],
                "Columns": cols_values[i],
                "SamplesPerPixel": 2,
                "BitsAllocated": 2,
            }
            for i in range(num_dicoms)
        }
    )
    from_zip_mock = mocker.patch.object(dicom.DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll

    size = calculate_decompressed_size(tmp_path)

    assert size == 0
    assert "Unable to estimate" in caplog.text


def test_calculate_decompressed_size_single_dicom(
    tmp_path, dcmcoll, mocker, mock_sniff, mock_is_zip, config
):
    config.reading_validation_mode = 2
    mock_sniff.return_value = True
    mock_is_zip.return_value = False
    coll = dcmcoll(
        **{
            "test1": {
                "Rows": 2,
                "Columns": 2,
                "SamplesPerPixel": 2,
                "BitsAllocated": 2,
            }
        }
    )
    dcm_coll_new = mocker.patch.object(dicom.DICOMCollection, "__new__")
    dcm_coll_init = mocker.patch.object(dicom.DICOMCollection, "__init__")
    dcm_coll_new.return_value = coll
    dcm_coll_init.return_value = None

    size = calculate_decompressed_size(tmp_path)

    assert size == 2
